# Microservice dynamic auto-scaling for Blog posts

This sample application will run blog posts in a dynamically scalable, auto load-balanced fashion.

## Components used:
- [Docker](https://www.docker.com/what-docker)
- [Registrator](https://github.com/gliderlabs/registrator) - Service Registrar
- [Consul](https://www.consul.io/intro/) - Service Registry
- [Consul-Template](https://github.com/hashicorp/consul-template) - Configuration file template tool
- [Nginx](https://www.nginx.com/resources/wiki/) - HTTP/Proxy server
- Blog posts application

## Application flow
1. Docker Compose stands up all the containers needed
2. Registrator listens to the Docker Event System and automatically registers new containers to the Consul service discovery engine
3. Consul Template, built into our Nginx container, queries Consul for available services and automatically generates an nginx.conf file, based on the available services.
4. Nginx then round-robins the load across all the available services that Consul says exist.
5. As Docker Compose scales the number of blog posts services, Registrator automatically registers these new instances in Consul.
6. Consul Template detects the changes in Consul, regenerates an updated nginx.conf with the new service endpoints, and then reloads the nginx configuration for updated _upstream_ servers.

## Pre-requisites

- Docker
- Docker Compose
- Clone this repo from https://bitbucket.org/duy-nguyen-2018/blog-posts.git

## Running this sample:

1. Set your Docker virtual machine or hostname IP to the MYHOST environment variable  
  ` export MYHOST=$(docker-machine ip default)`

2. Pull the pre-requisite images used in this sample  
  `docker-compose pull`

3. Build the images used in this sample  
  `docker-compose build`

4. Stand up the component container instances, in _detached mode_.  
  `docker-compose up -d`

5. Access the _Blog posts_ app via your browser at `http://${MYHOST}/`.  

6. Scale up the _Blog posts backend_ component.  
  `docker-compose scale backend=5` 

7. Back in your browser, reload the _Blog posts_ page at `http://${MYHOST}/`.  You can reload this page any number of times and the _nginx_ container will dynamically load-balance via round-robin across all the new _Blog posts backend_ container instances.

8. Backend documents at `http://${MYHOST}/api/docs`.

9. In _backend folder_ you can access _Backend Coverage_ via command `npm run test`.
