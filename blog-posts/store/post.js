export const state = () => ({
  list: {},
  selected: []
})

export const getters = {
  list(state) {
    return state.list
  },
  selected(state) {
    return state.selected
  }
}

export const mutations = {
  setList(state, payload) {
    state.list = payload
  },
  setSelected(state, payload) {
    state.selected = payload
  }
}

export const actions = {
  async fetchList({ commit }, { page, limit }) {
    const data = await this.$axios.$get('/api/posts', {
      params: {
        limit: limit === -1 ? 99999 : limit,
        page
      }
    })
    commit('setList', data)
  },

  async create({ commit }, payload) {
    await this.$axios.$post('/api/posts', payload)
  },
  async delete({ commit }, id) {
    await this.$axios.$delete(`/api/posts/${id}`)
  },
  async update({ commit }, { id, payload }) {
    console.log('update', payload)
    await this.$axios.$put(`/api/posts/${id}`, payload)
  }
}
