'use strict'

const AppException = use('AppException')

class PostRepo {
  static get inject () {
    return ['PostModel']
  }

  constructor (Post) {
    this.Post = Post
  }

  /**
   * return model's query
   *
   * @method query
   *
   * @return {Query}
   */
  query () {
    return this
      .Post
      .query()
  }

  /**
   * find post or fail by post ID
   *
   * @param {String} id post ID
   * @param {User} user User access
   *
   * @return {Post}
   */
  async findOrFail (id, user) {
    const query = this.query()
      .where(this.Post.primaryKey, id)

    if (user) {
      query.belongToUser(user.primaryKeyValue)
    }

    const post = await query.first()
    if (!post) {
      throw new AppException('Post not found with your given info', 404)
    }

    return post
  }

  /**
   * find post by slug and user
   *
   * @param {String} slug slug of post
   * @param {User} user User access
   *
   * @return {Post}
   */
  async findBySlug (slug, user) {
    const query = this.query()
      .whereSlug(slug)

    if (user) {
      query.belongToUser(user.primaryKeyValue)
    }

    const post = await query.first()
    if (!post) {
      throw new AppException('Post not found with your given info', 404)
    }

    return post
  }

  /**
   * update post by id
   *
   * @param {String} id post ID
   * @param {Object} payload data to update
   * @param {User} user User access
   *
   * @return {Post}
   */
  async update (id, payload, user) {
    const post = await this.findOrFail(id, user)
    post.merge(payload)

    await post.save()
    await post.reload()

    return post
  }

  /**
   * delete post by id
   *
   * @param {String} id post ID
   * @param {User} user User access
   *
   * @return {Post}
   */
  async delete (id, user) {
    const post = await this.findOrFail(id, user)
    await post.delete()

    return post
  }

  /**
   * get post by user with pagination
   *
   * @param {Number} page
   * @param {Number} limit
   * @param {User} user User access
   *
   * @return {Array} array of Post
   */
  paginate (page, limit, user) {
    const query = this.query()

    if (user) {
      query.belongToUser(user.primaryKeyValue)
    }
    return query
      .paginate(page, limit)
  }
}

module.exports = PostRepo
