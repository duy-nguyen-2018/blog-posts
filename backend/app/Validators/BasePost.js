'use strict'

class BasePost {
  get rules () {
    return {
      title: 'required',
      content: 'required'
    }
  }

  get sanitizationRules () {
    return {
      title: 'trim',
      content: 'escape'
    }
  }
}

module.exports = BasePost
