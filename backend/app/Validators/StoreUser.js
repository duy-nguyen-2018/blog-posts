'use strict'

class StoreUser {
  get rules () {
    return {
      username: 'required|unique:users,username',
      email: 'required|email|unique:users,email',
      password: 'required'
    }
  }

  get sanitizationRules () {
    return {
      username: 'trim',
      email: 'normalize_email'
    }
  }
}

module.exports = StoreUser
