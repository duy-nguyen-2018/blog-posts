'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with posts
 */
class PostController {
  static get inject () {
    return ['PostRepo']
  }

  constructor (PostRepo) {
    this.PostRepo = PostRepo
  }

  /**
   * Show a list of all posts.
   * GET posts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response, auth }) {
    const page = Number(request.input('page')) || 1
    const limit = Number(request.input('limit')) || 20
    const user = auth.user
    const posts = await this.PostRepo.paginate(page, limit, user)
    return response.json(posts)
  }

  /**
   * Create/save a new post.
   * POST posts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Auth} ctx.auth
   */
  async store ({ request, response, auth }) {
    const input = request.only(['title', 'content'])
    const user = auth.user
    input.userId = user.primaryKeyValue
    const post = await this.PostRepo.Post.create(input)
    return response.status(201).json(post)
  }

  /**
   * Display a single post.
   * GET posts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response, auth }) {
    const slug = params.slug
    const user = auth.user
    const post = await this.PostRepo.findBySlug(slug, user)
    return response.json(post)
  }

  /**
   * Update post details.
   * PUT or PATCH posts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response, auth }) {
    const id = params.id
    const user = auth.user
    const input = request.only(['title', 'content'])
    const post = await this.PostRepo.update(id, input, user)
    return response.json(post)
  }

  /**
   * Delete a post with id.
   * DELETE posts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response, auth }) {
    const id = params.id
    const user = auth.user
    await this.PostRepo.delete(id, user)
    return response.json({ id })
  }
}

module.exports = PostController
