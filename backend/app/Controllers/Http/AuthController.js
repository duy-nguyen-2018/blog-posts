'use strict'

class AuthController {
  static get inject () {
    return ['UserRepo']
  }

  constructor (UserRepo) {
    this.UserRepo = UserRepo
  }

  /**
   * register a new user
   *
   * @param {Context} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async register ({ request, response }) {
    const data = request.only(['username', 'email', 'password'])
    const user = await this.UserRepo.register(data)
    return response.status(201).json(user)
  }

  /**
   * login by email and password
   *
   * @param {Context} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Auth} ctx.auth
   */
  async login ({ request, response, auth }) {
    const email = request.input('email', '')
    const password = request.input('password', '')
    const token = await auth.attempt(email, password)
    return response.json(token)
  }

  /**
   * get logged in account info
   *
   * @param {Context} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Auth} ctx.auth
   */
  async getInfo ({ request, response, auth }) {
    const user = await auth.getUser()
    return response.json(user)
  }
}

module.exports = AuthController
