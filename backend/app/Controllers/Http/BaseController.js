'use strict'

class BaseController {
  static get inject () {
    return ['PostRepo']
  }

  constructor (PostRepo) {
    this.PostRepo = PostRepo
  }

  /**
   * Show a list of all posts.
   * GET posts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response, auth }) {
    const page = Number(request.input('page')) || 1
    const limit = Number(request.input('limit')) || 20
    const posts = await this.PostRepo.paginate(page, limit)
    return response.json(posts)
  }
}

module.exports = BaseController
