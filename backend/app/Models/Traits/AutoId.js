'use strict'

const crypto = require('crypto')
const biguint = require('biguint-format')

class AutoId {
  register (Model, customOptions = {}) {
    const defaultOptions = { padstr: '0', size: 15 }
    const options = Object.assign(defaultOptions, customOptions)
    Model.addHook('beforeCreate', async (modelInstance) => {
      modelInstance[Model.primaryKey] = biguint(AutoId.random(6), 'dec', options)
    })
  }

  static random (qty) {
    return crypto.randomBytes(qty)
  }
}

module.exports = AutoId
