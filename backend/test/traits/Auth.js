'use strict'

const jwt = require('jsonwebtoken')
const Config = use('Config')

const defaultUserData = {}

module.exports = function (suite) {
  const userToken = (uid = 'userid', data = defaultUserData) => {
    return jwt.sign({
      uid,
      data
    }, Config.get('app.appKey'))
  }

  suite.Context.getter('userToken', () => userToken)
}
