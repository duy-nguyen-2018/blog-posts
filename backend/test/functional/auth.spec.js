'use strict'

const { test, trait, beforeEach } = use('Test/Suite')('Auth')
const Factory = use('Factory')
const User = use('UserModel')
const userData = {
  username: 'username',
  email: 'username@email.com',
  password: 'password'
}

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await User.query().delete()
})

test('make sure user can register', async ({ assert, client }) => {
  const response = await client
    .post('auth/register')
    .send(userData)
    .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    username: userData.username,
    email: userData.email
  })
})

test('make sure user can login', async ({ assert, client }) => {
  await Factory.model('App/Models/User').create(userData)
  const response = await client
    .post('auth/login')
    .send({
      email: userData.email,
      password: userData.password
    })
    .end()

  response.assertStatus(200)
  const { token } = response.body
  assert.isString(token)
})

test('make sure user can get info', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create(userData)
  const response = await client
    .get('auth/me')
    .loginVia(user, 'jwt')
    .end()

  response.assertStatus(200)
  response.assertJSONSubset({
    username: userData.username,
    email: userData.email
  })
})
