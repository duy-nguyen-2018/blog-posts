'use strict'

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Swagger Information
  | Please use Swagger 2 Spesification Docs
  | https://swagger.io/docs/specification/2-0/basic-structure/
  |--------------------------------------------------------------------------
  */

  enable: true,
  specUrl: '/swagger.json',

  options: {
    swaggerDefinition: {
      info: {
        title: 'Adonis 💘 Swagger',
        version: '1.0.0'
      },

      basePath: '/api',

      // Example security definitions.
      securityDefinitions: {
        ApiKey: {
          description: 'JWT Authorization header using the Bearer scheme. Example: "Authorization: Bearer {token}"',
          name: 'Authorization',
          in: 'header',
          type: 'apiKey'
        }
      },

      responses: {
        UnauthorizedError: {
          description: 'API key is missing or invalid'
        }
      },

      tags: [
        {
          name: 'base',
          description: 'Base apis'
        },
        {
          name: 'auth',
          description: 'Register, login and get user info'
        },
        {
          name: 'posts',
          description: 'Everything about Post'
        }
      ]
    },

    // Path to the API docs
    // Sample usage
    // apis: [
    //    'docs/**/*.yml',    // load recursive all .yml file in docs directory
    //    'docs/**/*.js',     // load recursive all .js file in docs directory
    // ]
    apis: [
      'docs/**/*.yml'
      // 'start/routes.js'
    ]
  }
}
